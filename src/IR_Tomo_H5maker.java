import ij.IJ;
/*
import ij.Macro;
import ij.Menus;
import ij.Prefs;*/
import ij.io.OpenDialog;
import ij.plugin.PlugIn;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;

import javax.swing.JFileChooser;



//import javax.swing.filechooser.FileNameExtensionFilter;
/*
import ncsa.hdf.object.*; // the common object package
import ncsa.hdf.object.h5.*; // the HDF5 implementation
import ncsa.hdf.hdf5lib.*;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;*/
import ch.systemsx.cisd.hdf5.*;

import com.google.common.io.LittleEndianDataInputStream;

public class IR_Tomo_H5maker implements PlugIn {
	String BinarySpectrumPath;
	String BinaryImagePath;
	String BinaryFilePath;
	String waveString;
	String rootName;
	static int band;
	static int[] wavenum;
	static float[] dataarray;
	double wavestep;
	
	static int Xpixels; 
	static int Ypixels;
	
	/*
	 * Asks the user to select a directory of .dat/.bsp files//
	 */
	private String getTheInputName(){
		String input = "";
		JFileChooser fc = new JFileChooser(OpenDialog.getLastDirectory());
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fc.setDialogTitle("Select Directory of Binary Images");
		int returnVal = fc.showDialog(null, "Select");
		if (returnVal == JFileChooser.CANCEL_OPTION) {
			return "";
		}
		input = fc.getSelectedFile().getAbsolutePath();
		OpenDialog.setLastDirectory(fc.getSelectedFile().getPath().substring(0,fc.getSelectedFile().getPath().lastIndexOf(File.separator)+1));
		if(input == null){
			input = "";
		}
		return input;
	}
	
	/*Function for FIJI to run plugin, asks the user for an input directory. If a directory is selected,
	 *then make the h5 file (as an instance), then use the instance and create the degree groups from
	 *detecting dat files with "degrees" in their name. Finally, place the wavelength datasets inside
	 *each degree group */
	public void run(String arg) {
		BinaryFilePath = getTheInputName();
		if(BinaryFilePath == "")
		{
			return;
		}
		else{
			rootName = BinaryFilePath.substring(BinaryFilePath.lastIndexOf(File.separator)+1);
			makeH5File(BinaryFilePath+File.separator+rootName+".h5");
			File source = new File(BinaryFilePath);
			makeDegreeGroups(source,BinaryFilePath+File.separator+rootName+".h5");
			makeWaveDatasets(source,BinaryFilePath+File.separator+rootName+".h5");
		}
		
	}
	
	/*
	 * Main Function that the Java machine reads firsts as default, immediately calls run function//
	 */
	public static void main(String[] args) throws IOException {
		new IR_Tomo_H5maker().run(null);
	}
	
	/*
	 * Makes h5 file instance, if the h5 file exists, it will delete the old file!
	 */
	public void makeH5File(String path) {
		IJ.log("Creating "+ path);
		File f = new File(path);
		if (f.exists()){
			f.delete();
		}
		IHDF5Writer writer = HDF5Factory.open(path);
		IJ.log(path + " created!");
		writer.close();
	}
	
	/*
	 * Makes degree groups only from dat files with "degrees" in their name with zero padding, includes any other dat file like background.
	 */
	public void makeDegreeGroups(File sourcedirectory, String path) {
		
		FilenameFilter filter_dat1 = new datfilter();
		String[] filelist_dat = sourcedirectory.list(filter_dat1);
		String groupname = "";
		float number = 0f;
		DecimalFormat formatter = new DecimalFormat("0000.00");
		IHDF5Writer writer = HDF5Factory.open(path);
		for (int i = 0; i < filelist_dat.length; i++){
			if (filelist_dat[i].contains("degrees")){
				number = Float.parseFloat(filelist_dat[i].substring(0,filelist_dat[i].lastIndexOf("degrees")));
				groupname = formatter.format(number) + "degrees";
				System.out.println(groupname);
				writer.createGroup(groupname);
			}				
			else {
				writer.createGroup(filelist_dat[i].substring(0,filelist_dat[i].lastIndexOf('.')));
			}
		}
		
		writer.delete("__DATA_TYPES__");
		writer.close();
	}
	
	/*Makes wave datasets inside each group. Starts by first figuring out what the image dimensions of the datasets are from the dat file as well as the number of bands(wavelengths).
	 * The program uses an estimation method to find the wavelength values to name the datasets. The method is to find a wavestep and a starting wavenumber, multiply them together to get its wavelength. 
	 * Then propagate this method for all wavenumbers. Starting wavelength range and ending wavelength range comes from the bsp file. 
	 * Wavestep is calculated by:
	 * 
	 *  (Ending wavelength range (usually 3950) - starting wavelength range)/(number of bands)
	 *  
	 * Then each wavelength is calculated by:
	 * wavelength 1 = starting wavenum * wavestep
	 * wavelength 2 = starting wavenum+1 * wavestep
	 * wavelength 3 = starting wavenum+2 * wavestep
	 * ....
	 *
	 * After wavelengths are estimated, read the dat files and write the datasets for each wavelength within each degree group*/
	
	public void makeWaveDatasets(File sourcedirectory, String path) {
			
		FilenameFilter filter_dat1 = new datfilter();
		String[] filelist_dat = sourcedirectory.list(filter_dat1);
		FilenameFilter filter_bsp1 = new bspfilter();
		String[] filelist_bsp = sourcedirectory.list(filter_bsp1);
		
		IJ.log("Opening "+ path);
		IHDF5Writer writer = HDF5Factory.open(path);
			
		for (int i = 0; i < filelist_dat.length; i++){
			try {
				IJ.log("Reading pixel and band size from: " + filelist_dat[i]);
				if (i > 0){
					readPixelSize(sourcedirectory+File.separator+filelist_dat[i],sourcedirectory+File.separator+filelist_dat[i-1]);
					readBand(sourcedirectory+File.separator+filelist_dat[i],sourcedirectory+File.separator+filelist_dat[i-1]);
				} else if (i == 0 && filelist_dat.length > 1){
					readPixelSize(sourcedirectory+File.separator+filelist_dat[0],sourcedirectory+File.separator+filelist_dat[1]);
					readBand(sourcedirectory+File.separator+filelist_dat[0],sourcedirectory+File.separator+filelist_dat[1]);
				} else {
					readPixelSize(sourcedirectory+File.separator+filelist_dat[0],"");
					readBand(sourcedirectory+File.separator+filelist_dat[0],"");
				}
				
				if (filelist_bsp.length == filelist_dat.length && filelist_bsp[i].substring(0,filelist_bsp[i].lastIndexOf('.')).equals(filelist_dat[i].substring(0,filelist_dat[i].lastIndexOf('.')))){
					IJ.log("Reading wavenum from: " + filelist_bsp[i]);
					readWaveNumRange(sourcedirectory+File.separator+filelist_bsp[i]);
					wavestep = calculateWaveStep(FindRangeStart(sourcedirectory+File.separator+filelist_bsp[i]),FindRangeEnd(sourcedirectory+File.separator+filelist_bsp[i]), band);
				} else if (filelist_bsp.length < filelist_dat.length && filelist_bsp.length > 0) {
					IJ.log("Reading wavenum from: " + filelist_bsp[0]);
					readWaveNumRange(sourcedirectory+File.separator+filelist_bsp[0]);
					wavestep = calculateWaveStep(FindRangeStart(sourcedirectory+File.separator+filelist_bsp[0]),FindRangeEnd(sourcedirectory+File.separator+filelist_bsp[0]), band);
				} else if (filelist_bsp.length < 1){
					System.err.println("Missing Spectrum data");
				}
								
				for (int j = 0 ; j < band; j++){
					if (j == 0){
						waveString = String.format("%.4f", wavenum[j]*wavestep);
					}
					else if (j > 0 ) {
						waveString += ',';
						waveString += String.format("%.4f", wavenum[j]*wavestep);
					}
				}
				float number;
				String groupname = "";
				DecimalFormat formatter = new DecimalFormat("0000.00");
				if (filelist_dat[i].contains("degrees")){
					number = Float.parseFloat(filelist_dat[i].substring(0,filelist_dat[i].lastIndexOf("degrees")));
					groupname = formatter.format(number) + "degrees";
					writer.setStringAttribute(groupname, "wavelength", waveString);
				}
				waveString = null;
				
				IJ.log("Reading binary image data: " + BinaryFilePath+File.separator+filelist_dat[i]);
				//dataarray = readImageStack(BinaryFilePath+File.separator+filelist_dat[i]);
				//IJ.log("Writing binary image data: " + BinaryFilePath+File.separator+filelist_dat[i]);
				//write dataset images as H5 datasets
				
				float[][] waveImage = new float[Xpixels][Ypixels];
				for (int j = 0; j < band; j++){
					dataarray = readImage(BinaryFilePath+File.separator+filelist_dat[i], j);
					for (int k = 0; k < Ypixels; k++){
						for (int l=0; l < Xpixels; l++){
							waveImage[k][l] = dataarray[/*(j*Xpixels*Ypixels)+*/k*Xpixels+l];
						}
					}
					//IJ.log("Saving wavenumber " + wavenum[i]);
					if (filelist_dat[i].contains("degrees")){
						writer.writeFloatMatrix(groupname+'/'+rootName+'_'+String.format("%.4f", wavenum[j]*wavestep),waveImage);
					} else {
						writer.writeFloatMatrix(filelist_dat[i].substring(0,filelist_dat[i].lastIndexOf("."))+'/'+rootName+'_'+String.format("%.4f", wavenum[j]*wavestep),waveImage);

					}
					
				}
			}			
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		writer.delete("__DATA_TYPES__");
		IJ.log("Finished!");
		writer.close();
	}

	/*
	 * reads images' hexidecimal data from dat file
	 */
	public float[] readImage(String path, int band) throws IOException {
		dataarray = new float[Xpixels*Ypixels];
	    RandomAccessFile randomAccessFile = new RandomAccessFile(path, "r");
	    int byte_skip = band * Xpixels*Ypixels * 4;
	    randomAccessFile.seek(1020 +byte_skip);
		byte[] buf = new byte[Xpixels*Ypixels*4];
		randomAccessFile.readFully(buf,0,Xpixels*Ypixels*4); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		ByteBuffer buffer = ByteBuffer.wrap(buf);
		buffer.order(ByteOrder.LITTLE_ENDIAN);	
		for (int i = 0; i < Xpixels*Ypixels; i++){
			dataarray[i] = buffer.getFloat(i*4);
			//System.out.println(dataarray[i]);
		}
		randomAccessFile.close();
		return dataarray;
	}
	/*
	 * reads hexidecimal data for band(number of wavelengths) from dat file, if dat file is corrupted, the function will read from previous dataset
	 */
	public int readBand(String path, String previousPath) throws IOException {
		FileInputStream fs = new FileInputStream(path);
		LittleEndianDataInputStream le = new LittleEndianDataInputStream(fs);
		byte[] buf = new byte[4];
		le.skipBytes(9); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		le.readFully(buf, 0, 2); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		le.close();
		ByteBuffer buffer = ByteBuffer.wrap(buf);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		band = buffer.getInt();

		if (band == 0 && previousPath != ""){
			IJ.log("Dat file is missing band data");	
			IJ.log("Attempting to read band data from previous dat file");
			fs = new FileInputStream(previousPath);
			le = new LittleEndianDataInputStream(fs);
			buf = new byte[4];
			le.skipBytes(9); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(buf, 0, 2); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.close();
			buffer = ByteBuffer.wrap(buf);
			buffer.order(ByteOrder.LITTLE_ENDIAN);	
			band = buffer.getInt();
			
		} else if (band == 0 && previousPath == ""){
			IJ.log("Dat file is missing band data");	
		}
		IJ.log("Number of bands: "+band);
		return band;
	}
	

	/*
	 * reads hexidecimal data for image dimensions from dat file, if dat file is corrupted. the function will read from previous dataset
	 */
	public void readPixelSize(String path, String previousPath) throws IOException {
		FileInputStream fs = new FileInputStream(path);
		LittleEndianDataInputStream le = new LittleEndianDataInputStream(fs);
		byte[] x = new byte[4];
		byte[] y = new byte[4];
		le.skipBytes(24); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		le.readFully(x, 0, 1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		le.skipBytes(1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		le.readFully(y, 0, 1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		le.close();
		ByteBuffer xbuffer = ByteBuffer.wrap(x);
		ByteBuffer ybuffer = ByteBuffer.wrap(y);
		xbuffer.order(ByteOrder.LITTLE_ENDIAN);
		ybuffer.order(ByteOrder.LITTLE_ENDIAN);
		Xpixels = xbuffer.getInt();
		Ypixels = ybuffer.getInt();
		if (Xpixels == 0 && Ypixels == 0 && previousPath != ""){
			IJ.log("Dat file is missing pixel size data");
			IJ.log("Attempting to read pixel size data from previous dat file");
			fs = new FileInputStream(previousPath);
			le = new LittleEndianDataInputStream(fs);
			x = new byte[4];
			y = new byte[4];
			le.skipBytes(24); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(x, 0, 1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.skipBytes(1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(y, 0, 1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.close();
			xbuffer = ByteBuffer.wrap(x);
			ybuffer = ByteBuffer.wrap(y);
			xbuffer.order(ByteOrder.LITTLE_ENDIAN);
			ybuffer.order(ByteOrder.LITTLE_ENDIAN);
			Xpixels = xbuffer.getInt();
			Ypixels = ybuffer.getInt();		
		}
		else if (Xpixels == 0 && Ypixels == 0 && previousPath == ""){
			IJ.log("Dat file is missing pixel size data");
		}
		
		IJ.log("Image width = " + Xpixels);
		IJ.log("Image height = " + Ypixels);	
	}
	
	/*
	 * reads hexidecimal data for starting wavenumber range from bsp file
	 */
	public static void readWaveNumRange(String path) throws IOException {
		
		String[] dstring = {"04", "00", "00", "00", "44", "61", "74", "61"};

		byte[] data = readAllSpectrumBytes(path);
		byte[] pattern = new byte[dstring.length];
		
		for ( int i = 0; i < dstring.length; i++ ) {
			pattern[i] = (byte)Integer.parseInt(dstring[i], 16 );
			//IJ.log(pattern[i]);
		}
		
		HexMatch hx = new HexMatch();
		int index= hx.indexOf(data,pattern);
		IJ.log("wavenum index = " + index);
		
		FileInputStream fs = new FileInputStream(path);
		LittleEndianDataInputStream le = new LittleEndianDataInputStream(fs);
		byte[] buf = new byte[4];
		le.skipBytes(index+48); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		le.readFully(buf, 0, 1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
		ByteBuffer buffer = ByteBuffer.wrap(buf);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		wavenum = new int[band];
		wavenum[0] = buffer.getInt();
		if (wavenum[0] == 0){
			le.skipBytes(56-48); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(buf, 0, 1); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			buffer = ByteBuffer.wrap(buf);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			wavenum[0] = buffer.getInt();
		}
		for (int i = 1; i < band; i++){
			wavenum[i] = wavenum[i-1] + 1;
		}
		le.close();
	}
	
	/*
	 * reads hexidecimal data for starting wavelength range from bsp file
	 */
	public static int FindRangeStart(String path) throws IOException {
		FileInputStream fs = new FileInputStream(path);
		LittleEndianDataInputStream le = new LittleEndianDataInputStream(fs);
		int rangeStart = 400; //default at nominal//
		
		try {
			String[] range_start = {"52", "61", "6E", "67", "65", "20", "53", "74", "61", "72", "74"};
			
			byte[] data = readAllSpectrumBytes(path);
			byte[] pattern = new byte[range_start.length];
			
			for ( int i = 0; i < range_start.length; i++ ) {
				pattern[i] = (byte)Integer.parseInt(range_start[i], 16 );
				//IJ.log(pattern[i]);
			}
			 
			HexMatch hx = new HexMatch();
			int index= hx.indexOf(data,pattern);
			IJ.log("range start index = " + index);
			
			//Check Hex Matcher//
			byte[] check = new byte[range_start.length];
			le.skipBytes(index); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(check, 0, range_start.length); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			/*for ( int i = 0; i < range_end.length; i++ ) {
			IJ.log(check[i]);
			}*/
			
			byte[] waveStart = new byte[8];

			le.skipBytes(44);  //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(waveStart, 0, 8);
				
			for ( int i = 0; i < waveStart.length; i++ ) {
				if (waveStart[i] == 4){
					waveStart[i] = 0;
				}
			}
			
			String startnum = new String(waveStart, "UTF-8");
			rangeStart = Integer.parseInt(startnum.trim());
			IJ.log("rangeStart = "+rangeStart);
			le.close();
			
		} 	catch (java.io.EOFException eof) { System.err.print("Unable to finish reading spectrum file/finding range start!");}
		return rangeStart;
	}
	
	/*
	 * reads hexidecimal data for ending wavelength range from bsp file
	 */
	public static int FindRangeEnd(String path) throws IOException {
		FileInputStream fs = new FileInputStream(path);
		LittleEndianDataInputStream le = new LittleEndianDataInputStream(fs);
		int rangeEnd = 3949; //default at nominal//
		
		try {
			String[] range_end = {"52", "61", "6E", "67", "65", "20", "45", "6E", "64"};
			
			byte[] data = readAllSpectrumBytes(path);
			byte[] pattern = new byte[range_end.length];
			
			for ( int i = 0; i < range_end.length; i++ ) {
				pattern[i] = (byte)Integer.parseInt(range_end[i], 16 );
				//IJ.log(pattern[i]);
			}
			 
			HexMatch hx = new HexMatch();
			int index= hx.indexOf(data,pattern);
			IJ.log("range end index = " + index);
			
			//Check Hex Matcher//
			byte[] check = new byte[range_end.length];
			le.skipBytes(index); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(check, 0, range_end.length); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			/*for ( int i = 0; i < range_end.length; i++ ) {
				IJ.log(check[i]);
			}*/
			
			byte[] waveEnd = new byte[8];

			le.skipBytes(44);  //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.readFully(waveEnd, 0, 8);
				
			for ( int i = 0; i < waveEnd.length; i++ ) {
				if (waveEnd[i] == 4){
					waveEnd[i] = 0;
				}
			}
			
			String endnum = new String(waveEnd, "UTF-8");
			rangeEnd = Integer.parseInt(endnum.trim());
			IJ.log("rangeEnd = "+rangeEnd);
			le.close();
			
		} 	catch (java.io.EOFException eof) { System.err.print("Unable to finish reading spectrum file/finding range start!");}
		return rangeEnd;
	}
	
	/*
	 * Reads the full bsp file to search hexidecimal patterns for FindRangeStart, FindRangeEnd, and readWaveNumRange function//
	 */
	public static byte[] readAllSpectrumBytes(String path) throws IOException { 
		File bsp = new File(path); 
		byte[] bspbuf = new byte[(int) bsp.length()];
		FileInputStream fs = new FileInputStream(path);
		LittleEndianDataInputStream le = new LittleEndianDataInputStream(fs);
		try {
			le.readFully(bspbuf, 0, (int) bsp.length()); //WARNING these are magic numbers, please don't touch if you don't know what you're doing!//
			le.close();
		} 	catch (java.io.EOFException eof) {}
		return bspbuf;
	}
	
	public static double calculateWaveStep(int rangeStart, int rangeEnd, int bands) {
		return ((Math.min(rangeEnd, 3949)-rangeStart)/((double) bands-1));
	}
}

/*
 * Searches hexidecimal patterns for FindRangeStart, FindRangeEnd, and readWaveNumRange function//
 */
class HexMatch {
	/**
	 * Finds the first occurrence of the pattern in the text.
	 */
	public int indexOf(byte[] data, byte[] pattern) {
		int[] failure = computeFailure(pattern);

		int j = 0;
		if (data.length == 0) return -1;
		for (int i = 0; i < data.length; i++) {
			while (j > 0 && pattern[j] != data[i]) {
				j = failure[j - 1];
			}
			if (pattern[j] == data[i]) { j++; }
			if (j == pattern.length) {
				return i - pattern.length + 1;
			}
		}
		return -1;
	}

	/**
	 * Computes the failure function using a boot-strapping process,
	 * where the pattern is matched against itself.
	 */
	private int[] computeFailure(byte[] pattern) {
		int[] failure = new int[pattern.length];	
		int j = 0;
		for (int i = 1; i < pattern.length; i++) {
			while (j > 0 && pattern[j] != pattern[i]) {
				j = failure[j - 1];
			}
			if (pattern[j] == pattern[i]) {
				j++;
			}
			failure[i] = j;
		}
		return failure;
	}
}

/*
 * Filters file directory for .dat files only
 */
class datfilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		return (name.endsWith(".dat"));
	}
}
/*
 * Filters file directory for .bsp files only//
 */
class bspfilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		return (name.endsWith(".bsp"));
	}
}


# README #

## What is IR Tomo H5 Maker? ##

IR Tomo H5 Maker is a FIJI plugin written in Java that enables the IR beamline to write H5 files from binary DAT files obtained from IR tomography experiments.

## Simple Steps to get IR Tomo H5 Maker up and running on Eclipse ##

Step 1. Download and Install Eclipse IDE for Java EE Developers http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/keplersr2

Step 2. Once Eclipse is installed:

* Go to File > Import... > Projects from Git
* After clicking Projects from Git, select Clone URI (which is a broader term for  URL)
* Copy the URL in the browser but omit the word "overview" if it's there.
* Paste the URL into the Import Projects from Git dialog's URI textbox. 
* Authenticate yourself by inputting your Bitbucket username and password.
* After hitting Next, select the 'master' branch, then hit Next twice.
* Keep it as "Import as general project", hit Next then Finish.

Step 3. You'll see the ir-tomo-h5-maker project on your Project Explorer window. Right click it and select Configure > Convert to Maven Project.

Step 4. You'll notice a red x on the project icon now. Right click on the project icon again, but now select Properties. Then go to Java Build Path > Libraries. Select the 'Add External JARs...' button.

Step 5. Select the two JARs inside the resource folder within the project directory (which should be in C:/Users/[your username]/git/[project]/resource).

Step 6. Hit OK to exit out of the Properties window, and the project should now be run-able.

Step 7. To export a JAR of the project. Go to File > Export... > Runnable JAR file.

* Keep the Library Handling option as 'Extract required libraries into generated JAR'. Then hit Finish.